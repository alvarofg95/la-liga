import React, {lazy} from 'react';
const Login = lazy(() => import('pages/Login'));
const Home = lazy(() => import('pages/Home'));
const Detail = lazy(() => import('pages/Detail'));

export const ROUTES_PATHS = {
  LOGIN: '/login',
  HOME: '/',
  USER_DETAIL: '/user'
};

export const ROUTES = [
  {
    key: 'login',
    hashPath: ROUTES_PATHS.LOGIN,
    render: <Login />
  },
  {
    key: 'home',
    hashPath: ROUTES_PATHS.HOME,
    exact: true,
    requireAuth: true,
    render: <Home />
  },
  {
    key: 'detail',
    hashPath: ROUTES_PATHS.USER_DETAIL,
    requireAuth: true,
    render: <Detail />
  }
];
