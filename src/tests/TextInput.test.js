import React from 'react';
import {render, screen} from '@testing-library/react';
import TextInput from 'components/TextInput';

describe('CustomButton', () => {
  test('renders CustomButton component', () => {
    render(<TextInput value="test text" label="text example" />);
    screen.debug();
    expect(screen.queryByDisplayValue('test text')).toBeTruthy();
    expect(screen.queryAllByLabelText('text example')).toBeTruthy();
  });
});
