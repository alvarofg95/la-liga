import React from 'react';
import {render, screen} from '@testing-library/react';
import CustomButton from 'components/CustomButton';

describe('CustomButton', () => {
  test('renders CustomButton component', () => {
    render(<CustomButton text="test button" />);
    screen.debug();
    expect(screen.queryByText(/Signed in as/)).toBeNull();
    expect(screen.queryByText('test button')).toBeTruthy();
  });
});
