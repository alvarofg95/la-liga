import styled from 'styled-components';

const BorderImage = styled.img.attrs((props) => ({
  src: props.src,
  width: props.width,
  alt: props.alt || props.title,
  onClick: props.disabled ? null : props.onClick,
  className: props.disabled ? 'disabled' : '',
  title: props.title
}))`
  border-radius: ${(p) => (p.border ? '2rem' : '0')};
  cursor: ${(p) => (p.onClick ? 'pointer' : 'default')};
`;

export default BorderImage;
