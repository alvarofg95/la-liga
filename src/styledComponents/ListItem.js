import styled from 'styled-components';

export default styled.div`
  padding: 20px;
  display: flex;
  justify-content: space-between;
  border-radius: 1rem;
  margin: 10px;
  width: 20rem;
  cursor: pointer;
`;
