import styled from 'styled-components';

export default styled.div`
  list-style: none;
  border-radius: 2rem;
  padding: 0;
  width: 80%;
  margin: 0 auto;
`;
