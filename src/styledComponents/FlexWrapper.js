import styled from 'styled-components';

export default styled.div`
  display: inline-flex;
  justify-content: space-between;
  width: ${(props) => props.width || '100%'};
  margin-top: ${(props) => props.marginTop || '0px'};
`;
