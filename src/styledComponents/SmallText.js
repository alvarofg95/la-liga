import styled from 'styled-components';

export default styled.p`
  text-align: left;
  color: #fff;
  font-size: ${(props) => props.fontSize};
  width: ${(props) => props.width || '80%'};
  margin: 0 auto;
  line-height: ${(props) => props.lineHeight};
`;
