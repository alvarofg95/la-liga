import styled from 'styled-components';

export default styled.p.attrs({
  className: 'errorMessage'
})`
  padding: 1rem;
  border-radius: 1rem;
  font-size: 18px;
`;
