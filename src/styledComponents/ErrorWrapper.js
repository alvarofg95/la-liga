import styled from 'styled-components';

export default styled.div`
  position: absolute;
  top: 10%;
  width: 100%;
  text-align: center;
  display: flex;
  justify-content: center;
`;
