import styled from 'styled-components';

export default styled.div`
  width: 18rem;
  margin-bottom: 1rem;
  display: inline-grid;
`;
