import styled from 'styled-components';

export default styled.p`
  margin: 0 auto;
  text-align: center;
  width: fit-content;
  padding: 20px;
  border-radius: 2rem;
  position: fixed;
  bottom: 10rem;
  left: 50%;
  transform: translate(-50%, 0);
`;
