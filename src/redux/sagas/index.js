import {all, takeLatest} from 'redux-saga/effects';
import {
  GET_USERS_LIST,
  LOGIN_USER,
  LOAD_APP_INFO,
  SELECT_USER,
  UPDATE_USER,
  GET_USER,
  REMOVE_USER,
  LOGOUT,
  CHANGE_PAGE
} from 'redux/actions';
import {
  getUsersListWorker,
  loginWorker,
  loadAppInfoWorker,
  selectUserWorker,
  getUserWorker,
  updateUserWorker,
  removeUserWorker,
  logoutWorker
} from './workers';

export default function* rootSaga() {
  yield all([
    takeLatest(LOAD_APP_INFO, loadAppInfoWorker),
    takeLatest(LOGIN_USER, loginWorker),
    takeLatest(GET_USERS_LIST, getUsersListWorker),
    takeLatest(SELECT_USER, selectUserWorker),
    takeLatest(GET_USER, getUserWorker),
    takeLatest(UPDATE_USER, updateUserWorker),
    takeLatest(REMOVE_USER, removeUserWorker),
    takeLatest(LOGOUT, logoutWorker),
    takeLatest(CHANGE_PAGE, getUsersListWorker)
  ]);
}
