import {call, delay, put, select} from 'redux-saga/effects';
import {
  USERS_RESULT,
  LOGIN_RESULT,
  LOADED_APP_INFO,
  SET_SELECTED_USER,
  SET_ERROR,
  UPDATED_USER,
  CLEAR_NOTIFICATION,
  REMOVED_USER
} from 'redux/actions';
import {ROUTES_PATHS} from 'config/routes';
import saveDataToStorage from 'utils/saveDateToStorage';

export function* logoutWorker() {
  const history = yield select((state) => state.history);
  sessionStorage.clear();
  if (history && history.push) {
    history.push(ROUTES_PATHS.LOGIN);
  }
}

function removeUserRequest(userId) {
  return new Promise((resolve) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}`, {
      method: 'DELETE'
    })
      .then((res) => {
        res.text();
      })
      .then((response) => {
        resolve(response);
      });
  });
}

function* clearNotification() {
  yield delay(3000);
  yield put({type: CLEAR_NOTIFICATION});
}

export function* removeUserWorker({userId}) {
  yield call(removeUserRequest, userId);
  yield put({type: REMOVED_USER});
  yield call(clearNotification);
}

function updateUserRequest(userInfo) {
  return new Promise((resolve) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/${userInfo.id}`, {
      method: 'PUT',
      body: JSON.stringify({
        email: userInfo.email,
        first_name: userInfo.first_name,
        last_name: userInfo.last_name
      })
    })
      .then((res) => res.json())
      .then((response) => {
        resolve(!!response.updatedAt);
      });
  });
}

export function* updateUserWorker({userInfo}) {
  const response = yield call(updateUserRequest, userInfo);
  if (response) {
    yield put({type: UPDATED_USER});
    yield call(clearNotification);
  } else {
    yield put({type: SET_ERROR, payload: 'Error al actualizar el usuario'});
  }
}

function getUserRequest(userId) {
  return new Promise((resolve) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}`)
      .then((res) => res.json())
      .then((response) => {
        resolve(response.data);
      });
  });
}

export function* getUserWorker({userId}) {
  const response = yield call(getUserRequest, userId);
  if (response) {
    saveDataToStorage({selectedUser: response});
    yield put({type: SET_SELECTED_USER, payload: response});
  } else {
    yield put({type: SET_SELECTED_USER, payload: response});
    yield put({
      type: SET_ERROR,
      payload: 'No hay ningún usuario con ese ID, por favor, vuelva al listado de usuarios'
    });
  }
}

export function* selectUserWorker({userId}) {
  const history = yield select((state) => state.history);
  if (history && history.push) {
    history.push({
      pathname: ROUTES_PATHS.USER_DETAIL,
      search: `?id=${userId}`
    });
  }
}

function getUsersRequest({page}) {
  return new Promise((resolve) => {
    fetch(`${process.env.REACT_APP_API_URL}/users?page=${page}`)
      .then((res) => res.json())
      .then((response) => {
        resolve(response.data);
      });
  });
}

export function* getUsersListWorker({page = 1}) {
  const users = yield call(getUsersRequest, {page});
  saveDataToStorage({selectedUser: null, actualPage: page});
  yield put({type: USERS_RESULT, payload: {users, actualPage: page}});
}

function loginRequest(params) {
  return new Promise((resolve) => {
    fetch(`${process.env.REACT_APP_API_URL}/login`, {
      method: 'POST',
      body: JSON.stringify({
        ...params
      }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then((res) => res.json())
      .then((response) => {
        resolve(response);
      });
  });
}

export function* loginWorker({email, password, history}) {
  const response = yield call(loginRequest, {email, password});
  if (response && response.token) {
    saveDataToStorage({token: response.token});
    yield put({type: LOGIN_RESULT, payload: response.token});
    if (history && history.push) {
      history.push(ROUTES_PATHS.HOME);
    }
  } else {
    yield put({type: SET_ERROR, payload: 'El usuario no existe en la base de datos'});
  }
}

export function* loadAppInfoWorker({history}) {
  const data = sessionStorage.getItem('userInfo');
  const parsedData = JSON.parse(data);
  yield put({type: LOADED_APP_INFO, payload: {...parsedData, history}});
  if (parsedData && history && history.push) {
    if (parsedData.token) {
      if (parsedData.selectedUser) {
        // history.push(ROUTES_PATHS.USER_DETAIL.replace(':id', parsedData.selectedUser.id));
      } else {
        history.push(ROUTES_PATHS.HOME);
      }
    }
  }
}
