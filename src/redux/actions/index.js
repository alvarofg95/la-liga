export const LOAD_APP_INFO = 'LOAD_APP_INFO';
export const LOADED_APP_INFO = 'LOADED_APP_INFO';
export const LOGIN_USER = 'LOGIN_USER';
export const LOGIN_RESULT = 'LOGIN_RESULT';
export const USERS_RESULT = 'USERS_RESULT';
export const GET_USERS_LIST = 'GET_USERS_LIST';
export const SELECT_USER = 'SELECT_USER';
export const SET_SELECTED_USER = 'SET_SELECTED_USER';
export const GET_USER = 'GET_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const UPDATED_USER = 'UPDATED_USER';
export const REMOVE_USER = 'REMOVE_USER';
export const REMOVED_USER = 'REMOVED_USER';
export const LOGOUT = 'LOGOUT';
export const SET_ERROR = 'SET_ERROR';
export const CHANGE_PAGE = 'CHANGE_PAGE';
export const CLEAR_NOTIFICATION = 'CLEAR_NOTIFICATION';

export const loadAppInfoAction = (history) => ({
  type: LOAD_APP_INFO,
  history
});

export const loginUserAction = (params) => ({
  type: LOGIN_USER,
  ...params
});

export const getUsersAction = (page) => ({
  type: GET_USERS_LIST,
  page
});

export const selectUserAction = (userId) => ({
  type: SELECT_USER,
  userId
});

export const getUserAction = (userId) => ({
  type: GET_USER,
  userId
});

export const updateUserAction = (userInfo) => ({
  type: UPDATE_USER,
  userInfo
});

export const removeUserAction = (userId) => ({
  type: REMOVE_USER,
  userId
});

export const logoutAction = () => ({
  type: LOGOUT
});
