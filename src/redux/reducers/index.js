import {
  LOAD_APP_INFO,
  LOADED_APP_INFO,
  GET_USERS_LIST,
  USERS_RESULT,
  LOGIN_USER,
  LOGIN_RESULT,
  SELECT_USER,
  SET_SELECTED_USER,
  UPDATE_USER,
  REMOVE_USER,
  REMOVED_USER,
  UPDATED_USER,
  LOGOUT,
  SET_ERROR,
  CHANGE_PAGE,
  CLEAR_NOTIFICATION
} from 'redux/actions/index';

const initialState = {
  users: [],
  loading: false,
  token: null,
  loadedAppInfo: false,
  history: null,
  selectedUser: null,
  error: null,
  actualPage: 1,
  notification: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_APP_INFO:
      return {...state, loading: true};
    case LOADED_APP_INFO:
      return {...state, loading: false, loadedAppInfo: true, ...action.payload};
    case LOGIN_USER:
      return {...state, loading: true};
    case LOGIN_RESULT:
      return {...state, token: action.payload, loading: false, error: null};
    case GET_USERS_LIST:
      return {...state, loading: true, error: null};
    case USERS_RESULT:
      return {
        ...state,
        users: action.payload.users,
        actualPage: action.payload.actualPage,
        selectedUser: null,
        loading: false,
        error: null
      };
    case SELECT_USER:
      return {...state, loading: true};
    case SET_SELECTED_USER:
      return {...state, selectedUser: action.payload, loading: false};
    case UPDATE_USER:
      return {...state, loading: true};
    case UPDATED_USER:
      return {...state, loading: false, notification: 'Usuario actualizado correctamente'};
    case REMOVE_USER:
      return {...state, loading: true};
    case REMOVED_USER:
      return {...state, loading: false, notification: 'Usuario eliminado'};
    case SET_ERROR:
      return {...state, error: action.payload, loading: false};
    case CHANGE_PAGE:
      return initialState;
    case LOGOUT:
      return initialState;
    case CLEAR_NOTIFICATION:
      return {...state, notification: null};
    default:
      return state;
  }
};
export default reducer;
