import React, {Suspense} from 'react';
import PropTypes from 'prop-types';
import {Switch, Route} from 'react-router-dom';
import {ROUTES} from 'config/routes';
import RequireAuth from './RequireAuth';

const RouterControl = () => (
  <Suspense fallback={<div />}>
    <Switch>
      {ROUTES.map(({key, exact = false, hashPath, render, requireAuth}) => (
        <Route key={key} exact={exact} path={hashPath}>
          {requireAuth ? <RequireAuth component={render} path={hashPath} /> : render}
        </Route>
      ))}
    </Switch>
  </Suspense>
);

RouterControl.propTypes = {
  routes: PropTypes.arrayOf(
    PropTypes.shape({
      exact: PropTypes.bool,
      hashPath: PropTypes.string.isRequired,
      key: PropTypes.string.isRequired,
      render: PropTypes.element.isRequired,
      requireAuth: PropTypes.bool
    })
  )
};

export default RouterControl;
