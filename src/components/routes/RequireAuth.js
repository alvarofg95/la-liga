import React from 'react';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router';
import {connect} from 'react-redux';
import {ROUTES_PATHS} from 'config/routes';

const RequireAuth = ({component, loadedAppInfo, logged}) => {
  if (loadedAppInfo && !logged) {
    return <Redirect to={ROUTES_PATHS.LOGIN} />;
  }
  return component;
};

const mapStateToProps = ({loadedAppInfo, token}) => ({
  loadedAppInfo,
  logged: !!token
});

RequireAuth.propTypes = {
  component: PropTypes.element.isRequired,
  path: PropTypes.string.isRequired
};

export default connect(mapStateToProps)(RequireAuth);
