import React from 'react';

const CustomButton = ({text, type = 'button', disabled, onClick}) => (
  <button type={type} disabled={disabled} onClick={onClick}>
    {text}
  </button>
);

export default CustomButton;
