import React from 'react';
import SmallWrapper from 'styledComponents/SmallWrapper';

const TextInput = ({label, name, placeholder, type, value, onChange}) =>
  label ? (
    <SmallWrapper>
      <label htmlFor={name}>{label}</label>
      <input name={name} type={type} placeholder={placeholder} value={value} onChange={onChange} />
    </SmallWrapper>
  ) : (
    <input name={name} type={type} placeholder={placeholder} value={value} onChange={onChange} />
  );

export default TextInput;
