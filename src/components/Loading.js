import React from 'react';
import ReactLoading from 'react-loading';
import AbsoluteWrapper from 'styledComponents/AbsoluteWrapper';
import FixedWrapper from 'styledComponents/FixedWrapper';

const Loading = () => (
  <FixedWrapper className="loading-container">
    <AbsoluteWrapper className="loading-screen">
      <ReactLoading type="spin" />
    </AbsoluteWrapper>
  </FixedWrapper>
);

export default Loading;
