import React from 'react';
import {connect} from 'react-redux';
import {logoutAction} from 'redux/actions';
import CustomButton from './CustomButton';

const Header = ({logged, logout}) => {
  function handleLogout() {
    logout();
  }

  if (logged) {
    return (
      <header>
        <CustomButton text="Salir" onClick={handleLogout} />
      </header>
    );
  }
  return null;
};

const mapStateToProps = ({token}) => ({
  logged: !!token
});

const mapDispatchToProps = {
  logout: logoutAction
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
