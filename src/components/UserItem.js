import React from 'react';
import ListItem from 'styledComponents/ListItem';
import BorderImage from 'styledComponents/BorderImage';
import SmallText from 'styledComponents/SmallText';

const UserItem = ({id, avatar, first_name, last_name, onClick}) => (
  <ListItem
    onClick={() => onClick(id)}
    title={`Ver a ${first_name} ${last_name}`}
    className="userItem"
  >
    <BorderImage
      className="profileImg"
      border
      src={avatar}
      alt={`${first_name} ${last_name}`}
      width="20%"
    />
    <SmallText fontSize="25px" lineHeight="5rem">
      {first_name} {last_name}
    </SmallText>
  </ListItem>
);

export default UserItem;
