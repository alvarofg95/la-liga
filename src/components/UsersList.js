import React from 'react';
import {connect} from 'react-redux';
import {getUsersAction, selectUserAction} from 'redux/actions';
import BorderImage from 'styledComponents/BorderImage';
import FlexWrapper from 'styledComponents/FlexWrapper';
import ListWithoutDecoration from 'styledComponents/ListWithoutDecoration';
import UserItem from './UserItem';
import leftIcon from 'assets/left.svg';
import rightIcon from 'assets/right.svg';

const UsersList = ({users, actualPage, selectUser, getUsers}) => {
  function handleUserDetails(id) {
    selectUser(id);
  }

  function handleChangePage(page) {
    getUsers(page);
  }

  return (
    <div className="usersListContainer">
      <ListWithoutDecoration className="usersList">
        {users.map((user) => (
          <UserItem key={user.id} {...user} onClick={handleUserDetails} />
        ))}
      </ListWithoutDecoration>
      <FlexWrapper width="26rem" marginTop="4rem" className="btnWrapper">
        <BorderImage
          disabled={actualPage === 1}
          src={leftIcon}
          width="50px"
          onClick={() => handleChangePage(actualPage - 1)}
        />
        <span>{actualPage}</span>
        <BorderImage
          src={rightIcon}
          width="50px"
          disabled={actualPage === 2}
          onClick={() => handleChangePage(actualPage + 1)}
        />
      </FlexWrapper>
    </div>
  );
};

const mapStateToProps = (state) => ({actualPage: state.actualPage});

const mapDispatchToProps = {
  selectUser: selectUserAction,
  getUsers: getUsersAction
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);
