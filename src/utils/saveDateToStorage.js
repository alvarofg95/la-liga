export default function saveDataToStorage(data) {
  const storage = sessionStorage.getItem('userInfo');
  const storageData = storage ? JSON.parse(storage) : null;
  sessionStorage.setItem(
    'userInfo',
    JSON.stringify({
      ...storageData,
      ...data
    })
  );
}
