import React, {useReducer, useEffect, Fragment} from 'react';
import {connect} from 'react-redux';
import TextInput from 'components/TextInput';
import {updateUserAction, getUserAction, removeUserAction} from 'redux/actions';
import 'styles/detail.scss';
import {withRouter} from 'react-router';
import CustomButton from 'components/CustomButton';
import {ROUTES_PATHS} from 'config/routes';
import BorderImage from 'styledComponents/BorderImage';
import backIcon from 'assets/volver.svg';
import removeIcon from 'assets/eliminar.svg';
import FlexWrapper from 'styledComponents/FlexWrapper';
import ErrorMessage from 'styledComponents/ErrorMessage';
import NotificationWrapper from 'styledComponents/NotificationWrapper';

const initialState = {
  first_name: '',
  last_name: '',
  email: '',
  loaded: false
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'LOAD_INITIAL_STATE':
      return {
        ...action.userInfo,
        loaded: true
      };
    case 'CHANGE_INPUT':
      return {...state, [action.name]: action.value};
    default:
      return state;
  }
};

const Detail = ({selectedUser, error, notification, getUser, updateUser, removeUser, history}) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    if (!state.loaded) {
      const userId =
        history &&
        history.location &&
        history.location.search &&
        history.location.search.split('=')[1];
      getUser(userId);
    }
  }, [getUser, history, state.loaded]);

  useEffect(() => {
    if (!state.loaded && selectedUser) {
      dispatch({
        type: 'LOAD_INITIAL_STATE',
        userInfo: selectedUser
      });
    }
  }, [selectedUser, state.loaded]);

  function handleChange({target: {name, value}}) {
    dispatch({
      type: 'CHANGE_INPUT',
      name,
      value
    });
  }

  function handleGoHome(event) {
    event.preventDefault();
    history.push(ROUTES_PATHS.HOME);
  }

  function handleRemove(event) {
    event.preventDefault();
    if (window.confirm('Estás seguro de que quieres eliminar a este usuario?')) {
      removeUser(state.id);
    }
  }

  function handleSubmit(event) {
    event.preventDefault();
    const {first_name, last_name, email, id} = state;
    if (first_name && last_name && email && id) {
      updateUser({id, first_name, last_name, email});
    }
  }

  return (
    <div className="detailContainer">
      <FlexWrapper>
        <BorderImage
          className="icon"
          width="50px"
          src={backIcon}
          title="Volver a la lista"
          onClick={handleGoHome}
        />
        {!error ? (
          <BorderImage
            className="icon"
            width="50px"
            src={removeIcon}
            title="Eliminar a este usuario"
            onClick={handleRemove}
          />
        ) : null}
      </FlexWrapper>
      {notification ? (
        <NotificationWrapper className="notification">{notification}</NotificationWrapper>
      ) : null}
      <div className="detailFormContainer">
        <div className="detail">
          {error ? (
            <ErrorMessage>{error}</ErrorMessage>
          ) : (
            <Fragment>
              <BorderImage border src={selectedUser && selectedUser.avatar} width="45%" />
              <form onSubmit={handleSubmit}>
                <TextInput
                  name="first_name"
                  label="Nombre"
                  value={state.first_name}
                  onChange={handleChange}
                />
                <TextInput
                  name="last_name"
                  label="Apellido"
                  value={state.last_name}
                  onChange={handleChange}
                />
                <TextInput name="email" label="Email" value={state.email} onChange={handleChange} />
                <CustomButton text="Editar" type="submit" />
              </form>
            </Fragment>
          )}
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  selectedUser: state.selectedUser,
  error: state.error,
  notification: state.notification
});

const mapDispatchToProps = {
  getUser: getUserAction,
  updateUser: updateUserAction,
  removeUser: removeUserAction
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Detail));
