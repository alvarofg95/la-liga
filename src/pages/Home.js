import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {getUsersAction} from 'redux/actions';
import UsersList from 'components/UsersList';
import 'styles/home.scss';

const App = ({users, getUsers}) => {
  useEffect(() => {
    getUsers();
  }, [getUsers]);

  return (
    <div className="home">
      <UsersList users={users} />
    </div>
  );
};

const mapStateToProps = ({users}) => ({
  users
});

const mapDispatchToProps = {
  getUsers: getUsersAction
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
