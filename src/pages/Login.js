import React, {useEffect, useReducer} from 'react';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import {loginUserAction} from 'redux/actions';
import TextInput from 'components/TextInput';
import CustomButton from 'components/CustomButton';
import TitleText from 'styledComponents/TitleText';
import SmallWrapper from 'styledComponents/SmallWrapper';
import SmallText from 'styledComponents/SmallText';
import ErrorMessage from 'styledComponents/ErrorMessage';
import 'styles/login.scss';

const initialState = {
  email: '',
  password: ''
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'CHANGE_INPUT':
      return {...state, [action.name]: action.value};
    case 'CLEAN_STATE':
      return initialState;
    default:
      return state;
  }
};

const Login = ({error, loginUser, history}) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    if (error) {
      dispatch({type: 'CLEAN_STATE'});
    }
  }, [error]);

  function handleChange({target: {name, value}}) {
    dispatch({
      type: 'CHANGE_INPUT',
      name,
      value
    });
  }

  function handleSubmit(event) {
    event.preventDefault();
    if (state.email && state.password) {
      loginUser({email: state.email, password: state.password, history});
    }
  }

  return (
    <div className="loginContainer">
      <div className="login">
        <TitleText>Iniciar Sesión</TitleText>
        <SmallText fontSize="15px" width="18rem">
          Introduce tus datos para poder consultar la lista de alpinistas disponibles
        </SmallText>
        <form onSubmit={handleSubmit}>
          <TextInput
            name="email"
            label="Introducir email"
            type="email"
            onChange={handleChange}
            value={state.email}
          />
          <TextInput
            name="password"
            label="Introducir contraseña"
            type="password"
            value={state.password}
            onChange={handleChange}
          />
          <SmallWrapper>
            <CustomButton disabled={!state.email || !state.password} text="Entrar" type="submit" />
          </SmallWrapper>
        </form>
        {error && !state.email && !state.password ? <ErrorMessage>{error}</ErrorMessage> : null}
      </div>
    </div>
  );
};

const mapStateToProps = ({error}) => ({error});

const mapDispatchToProps = {
  loginUser: loginUserAction
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Login));
