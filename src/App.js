import React, {useEffect} from 'react';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import {loadAppInfoAction} from 'redux/actions';
import RouterControl from 'components/routes/RouterControl';
import Header from 'components/Header';
import Loading from 'components/Loading';

const App = ({loadedAppInfo, loading, loadAppInfo, history}) => {
  useEffect(() => {
    if (!loadedAppInfo) {
      loadAppInfo(history);
    }
  }, [loadedAppInfo, history, loadAppInfo]);

  return (
    <div className="container">
      <Header />
      <RouterControl />
      {loading ? <Loading /> : null}
    </div>
  );
};

const mapStateToProps = (state) => ({
  loadedAppInfo: state.loadedAppInfo,
  loading: state.loading
});

const mapDispatchToProps = {
  loadAppInfo: loadAppInfoAction
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(App));
