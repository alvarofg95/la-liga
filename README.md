# Descargar e instalar el proyecto
Para descargar el proyecto deberéis abrir el terminal y introducir el siguiente comando:
```
git clone https://gitlab.com/alvarofg95/la-liga.git
```
Una vez descargado tendréis que situaros en la carpeta del proyecto:
```
cd la-liga/
```
A continuación tenemos que instalar los paquetes necesarios
```
npm i
```
Una vez que tenemos todo instalado ejecutamos el proyecto
```
npm start
```
# Router
Para el router he utilizado un componente que suelo utilizar a menudo, en el cual manejo unos arrays de rutas, donde le indico el path, el componente que tiene que pintar cada ruta y si se necesita estar autenticado para acceder a ella o no. De esta forma controlo si el usuario está intentando acceder a una ruta protegida sin haber hecho login, en tal caso le redigirá a la vista de login.

# Funcionamiento de la aplicación

## Carga inicial de datos
Cuando se entra en la aplicación se comprueban si existen datos que almacenamos en algunas acciones de redux en el Session Storage, si encuentra el token de usuario por ejemplo nos deja entrar directamente a la página principal del listado de usuarios, en caso de no encontrarlo nos redigirá al login.

## Login
La aplicación nos redigirá automáticamente a la URL http://localhost:3000/login porque no hay un token que indique que se haya iniciado sesión.
Para hacer login se necesita un email de usuario válido y cualquier contraseña, para hacer la prueba se puede utilizar el email:
- eve.holt@reqres.in

Si pruebas con otro email la API devolverá error, y este será reflejado en la vista de login.

Si por el contrario entras con el email correcto guardará el token recibido en el store de Redux y avanzarás a la URL http://localhost:3000 donde encontraremos la vista principal con un listado de usuarios.
Una vez se haya hecho el login podremos observar que en la parte superior derecha aparecerá un botón para hacer Logout.

## Listado de usuarios
En la vista principal, encontraremos el listado de usuarios con paginación, si pulsamos sobre los botones verdes se accederá a la siguiente lista de usuarios. La carga del listado se realiza cada vez que se monta la página, una vez pintada la lista, podremos acceder a la información detallada de cada usuario solo con pinchar sobre el usuario que se desee.

## Detalle de usuario
Para acceder a la vista de detalle de cada usuario solo tenemos que pulsar sobre un usuario del listado, esta acción nos redigirá a la URL http://localhost:3000/user?id=<ID_USUARIO>, lo que esta página realiza es obtener el id del usuario mandado por parámetro en la URL y cuando la página ha terminado de montarse hace una petición GET al endpoint "https://reqres.in/api/users/<ID_USUARIO>", una vez recibidos los datos, los pintamos en la vista, en caso de que no exista un usuario con ese id la pantalla nos mostrará un error comentando lo sucedido y nos permitirá volver a la lista de usuarios
Dentro de esta vista podemos editar los datos del usuario como el nombre, apellido o email así como eliminar el usuario, ambas acciones devolverán un feedback para que el usuario sepa qué ha pasado, si ha ido bien o si ha ido mal.
La acción de borrar va precedida por un diálogo de confirmación, que deberá ser aceptado para que se ejecute la orden de borrado.

## Logout 
Para hacer logout hay que pulsar el boton de "salir" y el evento se encarga de limpiar la Session Storage, la propia aplicación se encarga de mandarte a la página de login si no encuentra token.

# Maquetación
La aplicación es totalmente responsive, se ha utilizado SASS como preprocesador de estilos y se han desarrollado algunos componentes con styled-components que son relativamente programables (pueden recibir ciertas propiedades).

# Formato de código
Se ha utilizado eslint como linter del código y prettier para su formato, en la raíz del proyecto encontramos los archivos .eslintrc.json y .prettierrc donde se encuentran las configuraciones de ambas herramientas.
También se ha incluído un git hook que hace las correcciones de formato necesarias en el pre-commit usando "husky" y "lint-stagged".

# Asincronía
Todo el tema de Request se ha desarrollado en los workers de Redux-Saga con Promises, y el flujo de errores y respuestas se ha manejado con el state de Redux. Consiguiendo que el usuario tenga conocimiento de que pasa en cada momento.
Cada vez que se hace una petición el state de redux se actualiza para mostrar visualmente al usuario un elemento tipo Spinner para indicarle que se está ejecutando una llamada a la API. Para conseguir este efecto he utilizado la librería "react-loading" ya que me parece bastante útil y cómoda de manejar.

# Tests
En los scripts del package.json podéis encontrar un script para ejecutar los tests:
```
npm run test
```
Estos tests comprueban el funcionamiento de dos de los componentes del proyecto, a continuación describo en qué consiste cada uno:
- TextInput: En los tests de este componente se comprueba que el componente adquiere el valor recibido por propiedades y al pasarle una propiedad label, inserta una etiqueta "<label>" en el componente.
- CustomButton: En el test de este componente se comprueba que el texto que se pinta en el botón es el mismo que recibe por propiedades. 